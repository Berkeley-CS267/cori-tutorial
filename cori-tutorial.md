# Getting Started with Cori

## 2FA and Log-In

You can set up the two-factor authentication (2FA) and log into Cori following the intructions [here](https://docs.nersc.gov/connect/mfa/).
Be sure to set up the `sshproxy` following the directions on that page to avoid typing your 2FA every time you log in.

## Command Line

Once logged into Cori, you are be presented with a [bash shell](https://en.wikipedia.org/wiki/Bash_(Unix_shell)).

Some important commands are:

```
ls                          # list files in currect directory
ls *.txt                    # list files in currect directory that end in .txt
cd [DIRNAME]                # change current directory
cd ~                        # change to your home directory
cd ..                       # go up one directory
cat [FILENAME]              # print file content
cat [FILENAME] | less       # print file content, scroll with arrows, quit with q
grep "needle" [FILENAME]    # searches for "needle" in file
man [COMMAND]               # display the manual for COMMAND, use q to quit
exit                        # close shell
```

You can go back to the last used command by using `↑` key.
Press `Enter` if you want to use the command, use `Backspace` to edit the
command before using it.
Use `Ctrl+R` to search through your command history.
Use `cat ~/.bash_history | less` to view your complete command history.

## Editor

One of the easiest ways to implement your homework is to directly change
the code on the server.
For this you need to use a command line editor like `nano` or `vim`.

For beginners we recommend taking your first steps with [nano](https://en.wikipedia.org/wiki/GNU_nano).
You can use it on Cori like this:

```.sh
[demmel@cori10 cs267_hw1_2019]$ module load nano
[demmel@cori10 cs267_hw1_2019]$ nano dgemm-blocked.c
```

Use `Ctrl+X` to exit.

For a more complete code editor try [vim](https://en.wikipedia.org/wiki/Vim_(text_editor))
which is loaded by default:

```.sh
[demmel@cori10 cs267_hw1_2019]$ vim dgemm-blocked.c
```

Use `Esc` and `:q` to exit.
(`:q!` if you want to discard changes).
Try out the [interactive vim tutorial](https://www.openvim.com/) to learn more.

## SSHFS

SSHFS is a utility that allows you to mount remote file systems to your local
system using SSH.  This has the advantage that you can use your favorite editor
such as Atom, Gnome Builder, Sublime Text, or Bloodshed Dev-C++ to edit files
on a remote file system.  To use SSHFS just install `sshfs` using your package
manager and then mount the remote file system to your local system.

```Bash
[xiii@shini ~]$ mkdir cori
[xiii@shini ~]$ sshfs brock@cori.nersc.gov: cori
Password + OTP:
[xiii@shini ~]$ cd cori
[xiii@shini cori]$ ls
intel  local  src
```

You can now direct your local editor to the files you want to edit in the
new mounted directory.

## Compiler

Compilers on Cori are selected using a "programming environment" module.  No matter
which compiler you use, you always compile on Cori using the `CC` (for C++) or `cc`
commands, which use special wrappers that invoke the compilers for you with some
specially tuned flags enabled (such as `-march=native`).

Cori uses Intel compilers by default.
You **must** switch to using the GNU compiler by changing the programming environment module
mentioned earlier.  To do this, you can use the command `module swap PrgEnv-intel PrgEnv-gnu`.
Now, when you invoke `CC` or `cc`, you will be using the GNU compilers.

Additionally, most homeworks require cmake version > 3.14, which can be loaded using the command `module load cmake`.
You can confirm your module configuration is correct by running `module list`.
For more details on modules, see the documentation on modules [here](https://docs.nersc.gov/environment/modules/).

## Compiler Explorer

You can examine the assembly produced for your code by using the `-S` flag in your compiler.

```Bash
[demmel@blasboss ~] cc -S hello.c -o hello.s
[demmel@blasboss ~]$ head hello.s
        .file   "hello.c"
        .text
        .section        .rodata
.LC0:
        .string "Hi, fam!"
```

While this can be useful, it's often cumbersome to constantly edit a sourcefile and then
have to recompile to view the assembly.  There's a nice tool called the
[GodBolt Compiler Explorer](https://godbolt.org/) that let's you interactively edit
code and view the assembly interactively as you optimize.  It also has a couple of
nice features that you won't find just using your local C/C++ compiler: it will color
code lines of code to visually match them up with the corresponding lines of assembly,
and it will use some advanced symbol lookup techniques to place more human-readable names
for your functions and variables in the assembly output.

## Acknowledgements

Thanks to Kevin Laeufer to create this tutorial.


